<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelSekolah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sekolah', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('npsn')->unique();
            $table->string('nama', 50);
            $table->string('email');
            $table->string('website');
            $table->string('notelp');
            $table->string('alamat');
            $table->string('nama_kepsek');
            $table->string('nip_kepsek');
            $table->string('nama_ops');
            $table->string('nip_ops');
            $table->string('logo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sekolah');
    }
}
