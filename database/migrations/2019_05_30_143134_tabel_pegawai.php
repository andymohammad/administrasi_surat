<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelPegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama', 50);
            $table->string('nip')->unique();
            $table->string('kode_jabatan', 20);
            $table->foreign('kode_jabatan', 20)
            ->references('kode_jabatan')->on('jabatan')->onDelete('CASCADE')
            ->onUpdate('CASCADE');
            $table->string('email')->unique();
            $table->string('notelp');
            $table->longtext('foto')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pegawai');
    }
}
