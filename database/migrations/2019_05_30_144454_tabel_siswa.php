<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelSiswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->string('nis')->unique();
            $table->string('nisn', 30)->unique();
            $table->string('nama', 50);
            $table->string('jk');
            $table->string('kode_jurusan', 25);
            $table->foreign('kode_jurusan', 25)
            ->references('kode_jurusan')->on('jurusan')
            ->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->string('kode_kelas');
            $table->foreign('kode_kelas')
            ->references('kode_kelas')->on('kelas')
            ->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->string('level');
            $table->string('agama');
            $table->string('email')->unique();
            $table->string('alamat');
            $table->string('notelp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswa');
    }
}
