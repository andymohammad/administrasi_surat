<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIAS &mdash; Laravel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('fa/css/all.min.css') }}" >
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('alt/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    @yield('style')
</head>    

<body class="hold-transition sidebar-mini">
    
    
    <!-- Site wrapper -->
    <div class="wrapper">
        
        @include('layout.header')
        
        
        @include('layout.leftside')
        <!-- =============================================== -->
        
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                @yield('konten')
                
            </section>
        </div>

        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 3.0.0-beta.1
            </div>
            <strong>Copyright &copy; 2014-2018 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
            reserved.
        </footer>
        
        
    </div>
    <!-- jQuery 3 -->
    
    <script src="/js/jquery/jquery.min.js"></script>
    <script>
        /** add active class and stay opened when selected */
        var url = window.location;
        
        // for sidebar menu entirely but not cover treeview
        $('ul.nav-sidebar a').filter(function() {
            return this.href == url;
        }).addClass('active');
        
        // for treeview
        $('ul.nav-treeview a').filter(function() {
            return this.href == url;
        }).parentsUntil(".nav-sidebar > .nav-treeview").addClass('menu-open') .prev('a').addClass('active');
    </script>
    
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- SlimScroll -->
    <script src="/js/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="/js/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('alt/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('alt/js/demo.js') }}"></script>
    <script src="/js/swal/sweetalert.min.js"></script>
    @yield('jskonten')
</body>
</html>