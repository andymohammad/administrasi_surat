<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIAS | Administrasi Surat</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/css/Ionicons/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/css/alt/AdminLTE.min.css">
    
    <link rel="stylesheet" href="/css/alt/skins/_all-skins.min.css">
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    
    @yield('style')
</head>
<body class="hold-transition skin-green sidebar-mini">
    

    <!-- Site wrapper -->
    <div class="wrapper">
        
        @include('layout.header')
        
        
        @include('layout.leftside')
        <!-- =============================================== -->
        
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    @yield('judulkonten')
                </h1>
                <!--<ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Examples</a></li>
                    <li class="active">Blank page</li>
                </ol>-->
            </section>
            
            <!-- Main content -->
            <section class="content">
                
                @yield('konten')
                
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        
        
        <div class="control-sidebar-bg"></div>
    </div>
    
    <!-- ./wrapper -->
    
    
    <!-- jQuery 3 -->
    <script src="http://code.jquery.com/jquery-2.0.3.min.js"></script> 
    <script src="/js/jquery/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="/js/bootstrap/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="/js/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="/js/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="/js/alt/adminlte.min.js"></script>
    <script src="/js/swal/sweetalert.min.js"></script>
    @yield('jskonten')
    <!-- AdminLTE for demo purposes 
        <script src="/js/alt/demo.js"></script>-->
        <script>
            var url = window.location;
            // for sidebar menu entirely but not cover treeview
            $('ul.sidebar-menu a').filter(function() {
                return this.href != url;
            }).parent().removeClass('active');
            
            // for sidebar menu entirely but not cover treeview
            $('ul.sidebar-menu a').filter(function() {
                return this.href == url;
            }).parent().addClass('active');
            
            // for treeview
            $('ul.treeview-menu a').filter(function() {
                return this.href == url;
            }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');
        </script>
        <script>
            $(document).ready(function () {
                $('.sidebar-menu').tree()
            })
        </script>
        
    </body>
    </html>
    