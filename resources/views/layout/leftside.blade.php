<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="../../index3.html" class="brand-link">
        {{--<img src="../../dist/img/AdminLTELogo.png"
        alt="AdminLTE Logo"
        class="brand-image img-circle elevation-3"
        style="opacity: .8">--}}
        <span class="brand-text font-weight-light">&nbsp;&nbsp;Administrasi Surat</span>
    </a>
    <!-- sidebar: style can be found in sidebar.less -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('files/photo/avatar5.png') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">ADMIN</a>
            </div>
        </div>
        
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                
                <li class="nav-item">
                    <a href="../widgets.html" class="nav-link">
                        <i class="nav-icon fa fa-tachometer-alt"></i>
                        <p>
                            Beranda
                            <span class="right badge badge-danger">New</span>
                        </p>
                    </a>
                </li>
                
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-database"></i>
                        <p>
                            Data Master
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('data.sekolah') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Data Sekolah</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('data.pegawai')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>User / Pegawai</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/panel/data-pegawai" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Kategori Surat</p>
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a href="/panel/data-pegawai" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Jenis Surat</p>
                            </a>
                        </li>
                        
                        
                    </ul>
                </li>
                
                <li class="nav-header">SURAT MASUK</li>
                
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>Arsip</p>
                    </a>
                </li>
                
                <li class="nav-header">SURAT KELUAR</li>
                
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>Agenda Surat</p>
                    </a>
                </li>
                
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>Arsip</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>