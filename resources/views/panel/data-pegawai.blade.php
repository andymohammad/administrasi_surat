@extends('layout.app')
@section('konten')
<!-- Default box -->
@include('panel.alert')
<div class="row">
    <div class="col-md-8 col-sm-8 col-xs-12">
        <div class="box box-success">
            <div class="box-body ">
                <div class="pull-right">
                    
                    <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#tambah_pegawai">
                        <i class="fa fa-plus-circle"></i><b> Tambah Pegawai</b>
                    </button>
                    
                    <button class="btn btn-sm bg-teal" data-toggle="modal" data-target="#upload">
                        <i class="fa fa-cloud-upload"></i><b> Upload Pegawai</b>
                    </button>
                    
                    <a href="/panel/data-pegawai/download"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#download">
                        <i class="fa fa-cloud-download"></i><b> Download Pegawai</b>
                    </button></a>
                    
                </div>
                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Nama Pegawai</th>
                            <th>NIP</th>
                            <th>e - Mail</th>
                            <th>Jabatan</th>
                            <th>Aksi</th>
                        </tr>
                        <tr>
                            @foreach ($pegawai as $b)
                            <td>{{$b->nama}}</td>
                            <td>{{$b->nip}}</td>
                            <td>{{$b->email}}</td>
                            <td><span class="label label-success">{{$b->jabatan}}</span></td>
                            <td>
                                <a href="" class="btn btn-sm btn-warning"> Edit</a>
                                <a href="#" class="btn btn-sm btn-danger pegawai" peg-nip="{{$b->nip}}"> Hapus</a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="box box-success">
            <div class="box-header">
                <br>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
                    <i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body ">
                <div class="pull-right">
                    <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#tambah_jabatan">
                        <i class="fa fa-plus-circle"></i><b> Tambah jabatan</b>
                    </button>
                </div>
                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Kode</th>
                            <th>Jabatan</th>
                            <th>Aksi</th>
                        </tr>
                        <tr>
                            @foreach ($jabatan as $a)
                            <td>{{$a->kode_jabatan}}</td>
                            <td>{{$a->jabatan}}</td>
                            <td>
                                <a href="#" class="btn btn-sm btn-warning"> Edit</a>
                                <a href="#" class="btn btn-sm btn-danger hapusjabatan" jabatan="{{$a->jabatan}}" kd-jabatan="{{$a->id}}"> Hapus</a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah_jabatan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Tambahkan Data Jabatan</h4>
            </div>
            <div class="modal-body">
                <form action="/panel/data-pegawai" method="POST" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Kode Jabatan</label>
                            <div class="col-sm-9">
                                <input type="text" name="kode" class="form-control" placeholder="Contoh : Waka Humas">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Jabatan</label>
                            <div class="col-sm-9">
                                <input type="text" name="jabatan" class="form-control" placeholder="Contoh : Wakil Kepala Sekolah Bidang Humas">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
        
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="tambah_pegawai">
    <div class="modal-dialog">
        <form action="/panel/data-pegawai/pegawai" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Tambahkan Data Pegawai</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="control-label">Nama Pegawai</label>
                                <input type="text" class="form-control" name="nama" required>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label">NIP / NIK</label>
                                <input type="text" class="form-control" name="nip" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-5">
                                <label class="control-label">Jabatan</label>
                                <select name="jabatan" id="" class="form-control" required>
                                    @foreach ($jabatan as $a)
                                    <option value="{{$a->kode_jabatan}}">{{$a->jabatan}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label">Nomor Telepon</label>
                                <input type="text" class="form-control" name="notelp" required>
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label">E-Mail</label>
                                <input type="email" class="form-control" name="email" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </form>
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="upload">
    <div class="modal-dialog">
        <form action="/panel/data-pegawai/upload" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Upload Data Pegawai</h4>
                </div>
                <div class="modal-body">
                    <div class="box">
                        <div class="callout callout-info">
                            <h4><i class="fa fa-info-circle"></i>Informasi</h4>
                            
                            <p>
                                Untuk melakukan upload pastikan data diisi sesuai format atau template yang
                                disediakan. Jika belum memiliki template Pegawai silahkan download 
                                <a href="#" class="btn btn-xs btn-success">disini</a>.
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input type="file" id="file" name="file" class="form-control" autofocus required>
                            <br><br><br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-cloud-upload"></i> Upload</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </form>
    </div>
    <!-- /.modal-dialog -->
</div>

@endsection

@section('jskonten')
<script>
    $('.hapusjabatan').click(function(){
        var kode_jabatan = $(this).attr('jabatan')
        swal({
            title: "Anda yakin ?",
            text: "Menghapus jabatan "+kode_jabatan+" ",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                swal("Data berhasil dihapus !", {
                    icon: "success",
                });
                window.location = "/panel/data-pegawai/"+kode_jabatan+"/hapusjabatan"
            }
        });
    })
    
    $('.pegawai').click(function(){
        var nip = $(this).attr('peg-nip')
        swal({
            title: "Anda yakin ?",
            text: "Apakah anda yakin ingin menghapus "+nip+" ini ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                swal("Data berhasil dihapus !", {
                    icon: "success",
                });
                window.location = "/panel/data-pegawai/"+nip+"/hapuspegawai"
            }
        });
    })
    
    
</script>
@endsection