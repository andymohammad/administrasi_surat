@extends('layout.app')
@section('style')
<link rel="stylesheet" href="/bootstrap3-editable/css/bootstrap-editable.css">
@endsection

@section('konten')
<?php
$a=DB::table('sekolah')->count()
?>
<div class="card card-primary card-outline" style="margin-top:10px;">
    <div class="card-header">
        <h3>Data Sekolah</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            
            @if ($a>0)
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="box box-success">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="/images/mastercard.png" alt="User profile picture">
                        @foreach ($view as $item)
                        <h3 class="profile-username text-center">
                            {{$item->nama}}
                        </h3>                
                        <p class="text-muted text-center">Motto Sekolah</p>
                        
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Siswa</b> <a class="pull-right">1,322</a>
                            </li>
                            <li class="list-group-item">
                                <b>Jurusan</b> <a class="pull-right">5</a>
                            </li>
                            <li class="list-group-item">
                                <b>Rombel</b> <a class="pull-right">287</a>
                            </li>
                        </ul>
                        @endforeach
                        <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            
            <div class="col-md-8 col-sm-8 col-xs-12">
                
            </div>
            <!-- /.box-body -->
        </div>        
    </div>
    
    @else
    
    <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="box box-success">
            <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle" src="/images/mastercard.png" alt="User profile picture">
                <h3 class="profile-username text-center">Nama Sekolah</h3>                
                <p class="text-muted text-center">Motto Sekolah</p>
                
                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>Siswa</b> <a class="pull-right">1,322</a>
                    </li>
                    <li class="list-group-item">
                        <b>Jurusan</b> <a class="pull-right">5</a>
                    </li>
                    <li class="list-group-item">
                        <b>Rombel</b> <a class="pull-right">287</a>
                    </li>
                </ul>
                
                <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    
    <div class="col-md-8 col-sm-8 col-xs-12">
        <div class="box box-success">
            <div class="card card-primary card-outline">
                <h3 class="box-title">Data Sekolah</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
                    <i class="fa fa-minus"></i></button>
                </div>
            </div>
            
            <div class="box-body">
                <form class="form-horizontal" method="POST" enctype="multipart/form-data" id="data-sekolah" action="/panel/data-sekolah/simpan">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-3 col-md-3 col-xs-4 ">Nama Sekolah</label>
                            <div class="col-sm-9 col-md-9 col-xs-8">
                                <input type="text" name="nama" class="form-control" placeholder="Nama Sekolah" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-md-3 col-xs-4 ">NPSN</label>
                            <div class="col-sm-9 col-md-9 col-xs-8">
                                <input type="text" name="npsn" class="form-control" placeholder="NPSN Sekolah" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-md-3 col-xs-4 ">Email</label>
                            <div class="col-sm-9 col-md-9 col-xs-8">
                                <input type="email" name="email" class="form-control"  placeholder="Email Sekolah" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-md-3 col-xs-4 ">Website</label>
                            <div class="col-sm-9 col-md-9 col-xs-8">
                                <input type="text" name="website" class="form-control" placeholder="Alamat Website Sekolah" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-md-3 col-xs-4 ">Telepon Sekolah</label>
                            <div class="col-sm-9 col-md-9 col-xs-8">
                                <input type="text" name="notelp" class="form-control" placeholder="031 - 7777777" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-md-3 col-xs-4 ">Alamat</label>
                            <div class="col-sm-9 col-md-9 col-xs-8">
                                <input type="text" name="alamat" class="form-control" placeholder="Alamat Lengkap Sekolah" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-md-3 col-xs-4 ">Penanggung Jawab</label>
                            <div class="col-sm-4 col-md-4 col-xs-8">
                                <input type="text" name="nama_kepsek" class="form-control" placeholder="Nama Kepala Sekolah" required>
                            </div>
                            <label class="col-sm-1 col-md-1 col-xs-4">NIP</label>
                            <div class="col-sm-4 col-md-4 col-xs-8">
                                <input type="text" name="nip_kepsek" class="form-control">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-3 col-md-3 col-xs-4">
                                <label>Operator</label>
                            </div>
                            <div class="col-sm-4 col-md-4 col-xs-8 ">
                                <input type="text" name="nama_ops" class="form-control" placeholder="Nama Operator" required>
                            </div>
                            <label class="col-sm-1 col-md-1 col-xs-4">NIP</label>
                            <div class="col-sm-4 col-md-4 col-xs-8">
                                <input type="text" name="nip_ops" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-md-3 col-xs-6">Motto Sekolah</label>
                            <div class="col-md-9 col-sm-9 col-xs-6">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-4">Logo Sekolah</label>
                            <div class="col-md-9 col-sm-9 col-xs-8">
                                <input type="file" name="logo" id="logo" class="form-control">
                            </div>
                        </div>
                    </div>
                    
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>        
    </div>
    @endif
</div>
    
    <div class="modal fade modal-sekolah" id="modal-sekolah">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <div class="modal-title">
                            <h4>Ubah Data Sekolah</h4>
                            
                        </div>
                    </div>
                    <div class="modal-body">
                        @foreach ($view as $a)
                        <form class="form-horizontal" method="POST" id="data-sekolah" action="/panel/data-sekolah/update"> 
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-3 col-md-3 col-xs-4 ">Nama Sekolah</label>
                                    <div class="col-sm-9 col-md-9 col-xs-8">
                                        <input type="text" class="form-control" name="nama" value="{{$a->nama}}" placeholder="Nama Sekolah" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-md-3 col-xs-4 ">NPSN</label>
                                    <div class="col-sm-9 col-md-9 col-xs-8">
                                        <input type="text" class="form-control" name="npsn" value="{{$a->npsn}}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-md-3 col-xs-4 ">Email</label>
                                    <div class="col-sm-9 col-md-9 col-xs-8">
                                        <input type="email" name="email" class="form-control" value="{{$a->email}}" placeholder="Email Sekolah" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-md-3 col-xs-4 ">Website</label>
                                    <div class="col-sm-9 col-md-9 col-xs-8">
                                        <input type="text" name="website" class="form-control" value="{{$a->website}}" placeholder="Alamat Website Sekolah" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-md-3 col-xs-4 ">Telepon Sekolah</label>
                                    <div class="col-sm-9 col-md-9 col-xs-8">
                                        <input type="text" name="notelp" class="form-control" value="{{$a->notelp}}" placeholder="031 - 7777777" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-md-3 col-xs-4 ">Alamat</label>
                                    <div class="col-sm-9 col-md-9 col-xs-8">
                                        <input type="text" name="alamat" class="form-control" value="{{$a->alamat}}" placeholder="Alamat Lengkap Sekolah" required>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 col-md-3 col-xs-4 ">Penanggung Jawab</label>
                                    <div class="col-sm-4 col-md-4 col-xs-8">
                                        <input type="text" name="nama_kepsek" class="form-control" value="{{$a->nama_kepsek}}" placeholder="Nama Kepala Sekolah" required>
                                    </div>
                                    <label class="col-sm-1 col-md-1 col-xs-4">NIP</label>
                                    <div class="col-sm-4 col-md-4 col-xs-8">
                                        <input type="text" name="nip_kepsek" value="{{$a->nip_kepsek}}" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-sm-3 col-md-3 col-xs-4">
                                        <label>Operator</label>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-xs-8 ">
                                        <input type="text" name="nama_ops" class="form-control" value="{{$a->nama_ops}}" placeholder="Nama Operator" required>
                                    </div>
                                    <label class="col-sm-1 col-md-1 col-xs-4">NIP</label>
                                    <div class="col-sm-4 col-md-4 col-xs-8">
                                        <input type="text" name="nip_ops" value="{{$a->nip_ops}}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 col-md-3 col-xs-6">Motto Sekolah</label>
                                    <div class="col-md-9 col-sm-9 col-xs-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-4">Logo Sekolah</label>
                                    <div class="col-md-9 col-sm-9 col-xs-8">
                                        <input type="file" name="logo" id="logo" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            @endforeach
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    @endsection
    
    @section('jskonten')
    
    <script src="/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.nama-sek').editable();
        });
    </script>
    <!--<script type="text/javascript">
        $(document).on('submit', '#data-sekolah', function(e){
            e.preventDefault();
            var form_data = new FormData($('#data-sekolah')[e]);
            $.ajax({
                type:'POST',
                
                processData:false,
                contentType:false,
                async:false,
                cache:false,
                data:form_data,
                success : function(response){
                    $('#hasil').val(response.data);
                }
            });
        });
    </script>-->
    @endsection