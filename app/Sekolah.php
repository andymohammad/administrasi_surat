<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sekolah extends Model
{
    protected $table='sekolah';
    protected $fillable=['npsn','nama','email','website','notelp','alamat','nama_kepsek','nip_kepsek',
    'nama_ops','nip_ops','logo' ];
}