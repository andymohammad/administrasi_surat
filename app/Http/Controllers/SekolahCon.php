<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sekolah;

class SekolahCon extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $a=Sekolah::all();
        return view('panel.data-sekolah', ['view'=>$a]);
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $d = new Sekolah;
        $d->npsn = $request->npsn;
        $d->nama = $request->nama;
        $d->email = $request->email;
        $d->website = $request->website;
        $d->notelp = $request->notelp;
        $d->alamat = $request->alamat;
        $d->nama_kepsek = $request->nama_kepsek;
        $d->nip_kepsek = $request->nip_kepsek;
        $d->nama_ops = $request->nama_ops;
        $d->nip_ops = $request->nip_ops;
        $d->logo = $request->logo;
        
        $d->save();
        return redirect()->back()->withInput();
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request)
    {
        $d = new Sekolah;
        $d->npsn = $request->npsn;
        $d->nama = $request->nama;
        $d->update();
        
        return back();
        
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy_jabatan(Request $request)
    {
        $a = \App\Jabatan::find($kode_jabatan);
        $a -> delete($a);
        return redirect('/panel/data-pegawai');
    }
}
