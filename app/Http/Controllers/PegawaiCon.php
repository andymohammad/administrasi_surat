<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jabatan; 
use App\Pegawai;



class PegawaiCon extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $a=Jabatan::all();
        $b=Pegawai::all();        
        return view('panel.data-pegawai', ['jabatan' => $a , 'pegawai' => $b]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_jabatan(Request $request)
    {
        $a = new Jabatan;
        $a->kode_jabatan = $request->kode;
        $a->jabatan = $request->jabatan;
        
        $a->save();
        return back();

    }

    public function store_pegawai(Request $request)
    {
        $a = new Pegawai;
        $a->nama = $request->nama;
        $a->nip = $request->nip;
        $a->kode_jabatan = $request->jabatan;
        $a->notelp = $request->notelp;
        $a->email = $request->email;
        $a->save();
        
        return back();
        
    }

    public function upload_pegawai(Request $request)
    {
        //
    }

    public function download_pegawai(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dest_jabatan($id)
    {
        $id = \App\Jabatan::find($id);
        $id->delete($id);
        return redirect('/panel/data-pegawai');
    }

    public function dest_pegawai($nip)
    {
        $nip=\App\Pegawai::find($nip);
        $nip->delete();
        return redirect('/panel/data-pegawai');
    }
}
