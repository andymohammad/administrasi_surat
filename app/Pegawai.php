<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawai';
    protected $primarykey = 'nip';
    protected $fillable = ['nama','nip','kode_jabatan','email','notelp','foto'];
}
