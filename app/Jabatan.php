<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $table='jabatan';
    protected $primarykey='kode_jabatan';
    protected $fillable=['kode_jabatan','jabatan'];
}
