<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('panel.beranda');
});

Route::prefix('panel/data-sekolah')->group(function(){
    Route::get('/', 'SekolahCon@index')->name('data.sekolah');
    Route::post('/simpan','SekolahCon@store');
    Route::post('/update', 'SekolahCon@update');
});

Route::prefix('panel/data-pegawai')->group(function(){
    Route::get('/', 'PegawaiCon@index')->name('data.pegawai');
    Route::post('/', 'PegawaiCon@store_jabatan');
    Route::get('/{id}/hapusjabatan', 'PegawaiCon@dest_jabatan');
    Route::get('/{nip}/hapuspegawai', 'PegawaiCon@dest_pegawai');
    Route::post('/pegawai', 'PegawaiCon@store_pegawai');
    Route::post('/upload', 'PegawaiCon@upload_pegawai');

});
//Route::get('/panel/data-pegawai/download', 'PegawaiCon@download_pegawai');